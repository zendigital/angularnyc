set -e
#rm -rf node_modules
npm update # vs install
gulp
echo GULP SUCCEEDED
echo REMOVING old site
rm -rf /var/www/angularnyc/*
echo copying newly built site live
cp -R dist/. /var/www/angularnyc/
echo "**********************"
echo AngularNYC BUILD FINISHED
echo "**********************"
